@extends('layouts.app')

@section('content')
<h1> {{ $user->name }} </h1>
<div class="col-6">
	<ul class="list-group">
		@foreach($follows as $follow)
			<li class="list-group-item">{{ $follow->name }} - {{ $follow->username }}</li>
		@endforeach
	</ul>
</div>
@endsection