@extends('layouts.app')

@section('content')
<h1>{{ $user->name }}</h1>
<div class="mt-2 mb-2">
	<a class="btn btn-primary btn-sm" href="/{{$user->username}}/follows">
		Sigue a <span class="badge badge-light">{{ $user->follows->count() }}</span>
	</a>

	<a class="btn btn-primary btn-sm" href="/{{$user->username}}/followers">
		Seguidores <span class="badge badge-light">{{ $user->followers->count() }}</span>
	</a>
</div>

@if(Auth::check())
	@if(Gate::allows('dms', $user))
		<form action="/{{ $user->username }}/dms" method="post">
			{{csrf_field()}}
			<input type="text" name="message" class="form-control mt-2 mb-2" />
			<button type="submit" class="btn btn-success mt-2 mb-2">
				Enviar DM
			</button>
		</form>

		@include('users.messages')

	@endif

	@if(Auth::user()->isFollowing($user))
		<form action="/{{$user->username}}/unfollow" method="post">
			{{ csrf_field() }}
			@if(session('success'))
				<span class="text-success">
					{{session('success')}}
				</span>
			@endif
			<button class="btn btn-danger"> Dejar de seguir</button>
		</form>
	@else
		<form action="/{{$user->username}}/follow" method="post">
			{{ csrf_field() }}
			@if(session('success'))
				<span class="text-success">
					{{session('success')}}
				</span>
			@endif
			<button class="btn btn-primary mt-2 mb-2"> Follow</button>
		</form>
	@endif
@endif
<div class="row my-4">
	@foreach($user->messages as $message)
		<div class="col-6">
			@include('messages.message')
		</div>
	@endforeach
</div>

@endsection
